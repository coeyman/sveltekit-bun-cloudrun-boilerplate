FROM oven/bun:1
WORKDIR /src
COPY / ./
RUN bun install
RUN bun run build
EXPOSE 8080
ENTRYPOINT ["bun", "start"]
